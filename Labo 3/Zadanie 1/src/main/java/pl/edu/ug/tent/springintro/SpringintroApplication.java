package pl.edu.ug.tent.springintro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ImportResource;

import pl.edu.ug.tent.springintro.domain.Person;

@SpringBootApplication
@ImportResource("classpath:beans.xml")
public class SpringintroApplication {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringintroApplication.class, args);

		Person person = (Person) context.getBean("prezes");
		System.out.println(person);
		person = (Person) context.getBean("wiceprezes");
		System.out.println(person);
		person = (Person) context.getBean("sekretarka");
		System.out.println(person);

	}

}
