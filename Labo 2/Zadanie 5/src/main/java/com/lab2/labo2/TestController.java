package com.lab2.labo2;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class TestController{
    @RequestMapping("/")
    public String index(){
        return "Jan Bielowka<br>" + "<img src='images/b_COFWmO_400x400.png' alt=''>";
    }
}