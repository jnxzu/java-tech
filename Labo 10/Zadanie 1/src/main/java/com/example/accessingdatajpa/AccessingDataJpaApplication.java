package com.example.accessingdatajpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.example.accessingdatajpa.Customer;
import com.example.accessingdatajpa.CustomerRepository;

@SpringBootApplication
public class AccessingDataJpaApplication {

	private static final Logger log = LoggerFactory.getLogger(AccessingDataJpaApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(AccessingDataJpaApplication.class);
	}

	@Bean
	public CommandLineRunner demo(CustomerRepository repository) {
		return (args) -> {
			// save a few customers
			Date utilDate = new Date();
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy");
			repository.save(new Customer("Jack", "Bauer", 0.0, new java.sql.Date(utilDate.getTime())));
			repository.save(new Customer("Chloe", "O'Brian", 0.0, new java.sql.Date(utilDate.getTime())));
			repository.save(new Customer("Kim", "Bauer", 0.1, new java.sql.Date(df.parse("02-04-2015").getTime())));
			repository.save(new Customer("David", "Palmer", 0.0, new java.sql.Date(utilDate.getTime())));
			repository.save(new Customer("Michelle", "Dessler", 0.0, new java.sql.Date(utilDate.getTime())));

			// fetch all customers
			log.info("Customers found with findAll():");
			log.info("-------------------------------");
			for (Customer customer : repository.findAll()) {
				log.info(customer.toString());
			}
			log.info("");

			// fetch an individual customer by ID
			Customer customer = repository.findById(1L);
			log.info("Customer found with findById(1L):");
			log.info("--------------------------------");
			log.info(customer.toString());
			log.info("");

			// fetch customers by last name
			log.info("Customer found with findByLastName('Bauer'):");
			log.info("--------------------------------------------");
			repository.findByLastName("Bauer").forEach(bauer -> {
				log.info(bauer.toString());
			});
			log.info("");

			// fetch customer by money
			log.info("Customer found with findByMoney(0.1):");
			log.info("--------------------------------------------");
			customer = repository.findByMoney(0.1);
			log.info(customer.toString());
			log.info("");

			// fetch customer by date
			log.info("Customer found with findByDate(02-04-2015):");
			log.info("--------------------------------------------");
			customer = repository.findByData(new java.sql.Date(df.parse("02-04-2015").getTime()));
			log.info(customer.toString());
			log.info("");
		};
	}

}
