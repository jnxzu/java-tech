package com.example.accessingdatajpa;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String firstName;
	private String lastName;
	private Double money;
	private Date data;

	public Customer(String firstName, String lastName, Double money, Date data) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.money = money;
		this.data = data;
	}
}
